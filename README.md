# Debian

|                       |                                                      |
|                   ---:|---                                                   |
| kernel                | linux                                                |
| libc                  | glib?                                               |
| compiler              | gcc?                                                 |
| init                  | SystemD?                                             |
| shell                 | bash                                                 |
| Unix-like basic tools | coreutils?                                           |
| man                   | Gnu? man?                                            |
| man l10n              | To check if it works with lxc-doc?                   |


## Used packages
* [procps](https://packages.debian.org/en/procps)


## Debian websites
* [debian.org](https://debian.org/)
* [packages.debian.org](https://packages.debian.org/)
* [manpages.debian.org](https://manpages.debian.org/)


## Docker
* [*Building Debian Packages with buildah*
  ](https://tauware.blogspot.com/2020/04/building-packages-with-buildah-in-debian.html)
  2020-04 Reinhard Tartler
* [*Running services inside Docker containers*
  ](https://wiki.debian.org/Docker) (2019)
* [*Debian Working System for Docker*
  ](https://github.com/jgoerzen/docker-debian-base) (2019) jgoerzen


## LXC
* [wiki.debian.org](https://wiki.debian.org/) > [LXC](https://wiki.debian.org/LXC)


## LXD
* [wiki.debian.org](https://wiki.debian.org/) > [LXD](https://wiki.debian.org/LXD)
* [ITP: lxd](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=768073)